jest.unmock('../../src/net/http_request');

import {HttpRequest} from "../../src/net/http_request";

const headers = {'Accept': 'application/json', 'Content-Type': 'application/json', 'X-Session-Token': 42};

describe('http', () => {
    it('GET request can be made', () => {
        const request = new HttpRequest(42, {url: '/', method: 'GET'});
        const spy = spyOn(request, 'requestInvoker');

        request.request(null);

        expect(spy).toHaveBeenCalledWith('/', {
            method: 'GET',
            headers
        });
    })
});