jest.unmock('../src/session');
import {Config} from "../src/config";
import {Session} from "../src/session";

let addEventListener, open, send, setRequestHeader;

function XHRmock(mock) {
    const backXHR = window.XMLHttpRequest;

    if (mock) {
        addEventListener = jest.genMockFn();

        open = jest.genMockFn();

        // be aware we use *function* because we need to get *this*
        // from *new XmlHttpRequest()* call
        send = jest.genMockFn().mockImpl(function (params) {
            //onload = this.onload.bind(this);
            //onerror = this.onerror.bind(this);
        });

        setRequestHeader = jest.genMockFn();

        const xhrMockClass = function () {
            return {
                addEventListener,
                open,
                send,
                setRequestHeader
            };
        };

        window.XMLHttpRequest = jest.genMockFn().mockImpl(xhrMockClass);
    } else {
        window.XMLHttpRequest = backXHR;
    }
}

describe('session.getDeviceSession', function () {
    it('checks XHR parameters', function () {
        XHRmock(true);

        const session = new Session(Config.EPIX_ACCESS_TOKEN);
        const params = JSON.stringify({apikey: Config.EPIX_ACCESS_TOKEN});

        session.getDeviceSession();

        expect(open).toBeCalledWith('POST', Config.BASE_URL + 'sessions', false);
        expect(setRequestHeader).toBeCalledWith('Content-type', 'application/json');
        expect(setRequestHeader).toBeCalledWith('Accept', 'application/json');
        expect(send).toBeCalledWith(params);

        XHRmock(false);
    });
});

describe('session.getDeviceSession', function () {
    it('checks new device session response', function () {
        const session = new Session(Config.EPIX_ACCESS_TOKEN);
        const sessionMock = {
            device_session: {
                session_token: '12345',
                guid: '54321',
                service_level: 'EAU',
                parental_level: null,
                user: null,
                test: null
            }
        };
        spyOn(session, 'getDeviceSession').and.returnValue(sessionMock);
        const testSession = session.getDeviceSession();
        expect(testSession).toEqual(sessionMock);
    });
});