jest.unmock('../../src/models/movie_playhead_position_model.js');

import {MoviePlayheadPositionModel} from "../../src/models/movie_playhead_position_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"movie":{"id": 42,"position": 0}}');

        expect(new MoviePlayheadPositionModel(42, 0)).toEqual(MoviePlayheadPositionModel.fromJSON(jsonObject));
    })
});