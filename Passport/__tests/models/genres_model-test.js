import {ContentModelSelector} from '../../src/helpers/content_model_selector';
import {jsonCanBeParsedTest} from './helpers/test_helper_functions';
import {genresTestVars} from './helpers/test_helper_variables';

jest.unmock('../../src/models/genres_model.js');
jest.unmock('../../src/models/collection_content_model.js');
jest.unmock('../../src/helpers/content_model_selector.js');
jest.unmock('../../src/models/collection_item.js');
jest.unmock('./helpers/test_helper_functions');
jest.unmock('./helpers/test_helper_variables');
jest.unmock('../../src/helpers/arrays.js');

jsonCanBeParsedTest(genresTestVars.genresJson, genresTestVars.genresInstance, genresTestVars.genresClass);