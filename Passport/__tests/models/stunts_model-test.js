jest.unmock('../../src/models/stunts_model.js');
jest.unmock('../../src/models/collection_item.js');
jest.unmock('../../src/models/collection_content_model.js');
jest.unmock('../../src/helpers/content_model_selector');
jest.unmock('./helpers/test_helper_functions');
jest.unmock('./helpers/test_helper_variables');
jest.unmock('../../src/helpers/arrays.js');

import {StuntsModel} from "../../src/models/stunts_model";
import {CollectionItem} from "../../src/models/collection_item";
import {CollectionContentModel} from "../../src/models/collection_content_model";
import {ContentModelSelector} from '../../src/helpers/content_model_selector';

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"stunt":{' +
            '"meta": {"total_count": 2},' +
            ' "items": [{"type":"stunt", "content":{"id":1, "title":"My Stunt", "thumbnail":null}, "action":"navigate", "target":"default"}, {"type":"stunt", "content":{"id":2, "title":"My Stunt2", "thumbnail":null}, "action":"navigate", "target":"default"}]}}');

        expect(new StuntsModel({total_count: 2}, [new CollectionItem("stunt", new CollectionContentModel(1, "My Stunt", null), "navigate", "default"), new CollectionItem("stunt", new CollectionContentModel(2, "My Stunt2", null), "navigate", "default")])).toEqual(StuntsModel.fromJSON(jsonObject));
    })
});