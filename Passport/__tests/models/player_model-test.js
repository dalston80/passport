jest.unmock('../../src/models/player_model.js');

import {PlayerModel} from "../../src/models/player_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"url": "//posters.epix.com/13601/playerposter.jpg","thumb": "//posters.epix.com/13601/playerposter/200x114.jpg"}');

        expect(new PlayerModel("//posters.epix.com/13601/playerposter.jpg", "//posters.epix.com/13601/playerposter/200x114.jpg")).toEqual(PlayerModel.fromJSON(jsonObject));
    })
});