jest.unmock('../../src/models/extra_model.js');
jest.unmock('../../src/models/movie_list_item.js');
jest.unmock('../../src/models/extras_item.js');
jest.unmock('../../src/helpers/arrays.js');

import {ExtraModel} from "../../src/models/extra_model";
import {MovieListItem} from "../../src/models/movie_list_item";
import {ExtrasItem} from "../../src/models/extras_item";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"collection":{"id":42, "movies":{"10":{"id":3}}, "items":[{"id":1, "title":null, "duration":null, "description":null, "hlspath":null, "poster":null, "movie_id":10, "item":null}]}}');

        expect(new ExtraModel(42, {"10": new MovieListItem(3)}, [new ExtrasItem(1, null, null, null, null, null, 10, null)])).toEqual(ExtraModel.fromJSON(jsonObject));
    })
});
