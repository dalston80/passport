jest.unmock('../../src/models/posters_model.js');
jest.unmock('../../src/models/player_model.js');

import {PostersModel} from "../../src/models/posters_model";
import {PlayerModel} from "../../src/models/player_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"thumb": "//posters.epix.com/13601/poster/113x169.jpg","small": "//posters.epix.com/13601/poster/214x321.jpg","medium": "//posters.epix.com/13601/poster/255x383.jpg","large": "//posters.epix.com/13601/poster/506x760.jpg","chromecast": null,"player": {"url": "//posters.epix.com/13601/playerposter.jpg","thumb": "//posters.epix.com/13601/playerposter/200x114.jpg"},"hero": "//posters.epix.com/13601/16x9-story-art-detail.jpg","story_art": "//posters.epix.com/13601/16x9-story-art.jpg"}');

        expect(new PostersModel(
            "//posters.epix.com/13601/poster/113x169.jpg",
            "//posters.epix.com/13601/poster/214x321.jpg",
            "//posters.epix.com/13601/poster/255x383.jpg",
            "//posters.epix.com/13601/poster/506x760.jpg",
            null,
            new PlayerModel("//posters.epix.com/13601/playerposter.jpg", "//posters.epix.com/13601/playerposter/200x114.jpg"),
            "//posters.epix.com/13601/16x9-story-art-detail.jpg",
            "//posters.epix.com/13601/16x9-story-art.jpg"
        )).toEqual(PostersModel.fromJSON(jsonObject));
    })
});