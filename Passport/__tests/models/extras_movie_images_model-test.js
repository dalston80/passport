jest.unmock('../../src/models/extras_movie_images_model.js');

import {ExtrasMovieImagesModel} from "../../src/models/extras_movie_images_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{' +
            '"id": 434461,' +
            '"bitly": null,' +
            '"original_url": "//content.epixhd.com/webassets/static/movies/images/13601/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434461.jpg",' +
            '"websized_url": "//content.epixhd.com/webassets/static/movies/images/websized/13601/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434461.jpg",' +
            '"thumbnail_url": "//content.epixhd.com/webassets/static/movies/images/13601/thumbs/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434461.jpg"}'
        );

        expect(new ExtrasMovieImagesModel(434461, null, "//content.epixhd.com/webassets/static/movies/images/13601/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434461.jpg", "//content.epixhd.com/webassets/static/movies/images/websized/13601/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434461.jpg", "//content.epixhd.com/webassets/static/movies/images/13601/thumbs/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434461.jpg")).toEqual(ExtrasMovieImagesModel.fromJSON(jsonObject));
    })
});