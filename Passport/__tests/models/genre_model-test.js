jest.unmock('../../src/models/genre_model.js');
jest.unmock('../../src/models/genre_item.js');
jest.unmock('../../src/models/movie_list_item.js');
jest.unmock('../../src/helpers/arrays.js');

import {GenreModel} from "../../src/models/genre_model";
import {GenreItem} from "../../src/models/genre_item";
import {MovieListItem} from "../../src/models/movie_list_item";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{' +
            '"meta": {"total_count": 1},' +
            ' "genre": {"id":1, "name":"My Genre", "movies":[{"id":855,"title":"Top Gun","short_name":"top-gun","release_year":1986,"mpaa_rating":"PG","duration":6575.29,"genres":["Recently Added","Action","Drama"]}]}}');

        expect(new GenreModel({total_count: 1}, new GenreItem(1, "My Genre", [new MovieListItem(855, "Top Gun", "PG", 1986, 6575.29, "top-gun", undefined, ["Recently Added", "Action", "Drama"], undefined, undefined, undefined, undefined)]))).toEqual(GenreModel.fromJSON(jsonObject));
    })
});