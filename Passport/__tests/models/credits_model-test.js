jest.unmock('../../src/models/credits_model.js');

import {CreditsModel} from "../../src/models/credits_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"type": "cast", "name": "My Name", "role": "My Role"}');

        expect(new CreditsModel("cast", 'My Name', "My Role")).toEqual(CreditsModel.fromJSON(jsonObject));
    })
});