jest.unmock('../../src/models/collection_item.js');
jest.unmock('../../src/models/movie_content_model.js');
jest.unmock('../../src/helpers/content_model_selector');

import {CollectionItem} from "../../src/models/collection_item";
import {MovieContentModel} from "../../src/models/movie_content_model";
import {ContentModelSelector} from '../../src/helpers/content_model_selector';

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"type": "movie","content": {"id": 1,"title": "Desperately Seeking Susan","thumbnail": "//posters.epix.com/1/playerposter/200x114.jpg","short_name": "desperately-seeking-susan","release_year": 1985,"mpaa_rating": "PG-13","duration": 7839.57,"rotten_tomatoes_review": {"id": 11353,"movie_id": 1,"urls": {"rottentomatoes":"http://www.rottentomatoes.com/m/desperately_seeking_susan","flixster":"http://www.flixster.com/movie/desperately-seeking-susan"},"criticScore": {"score":87,"url":"http://www.rottentomatoes.com/m/desperately_seeking_susan#reviews","cerifiedFresh":false,"rotten":false},"fanScore": {"score":62,"url":"http://www.rottentomatoes.com/m/desperately_seeking_susan/reviews/?type=user"}},"background": "//posters.epix.com/1/playerposter.jpg"},"action": "navigate","target": "default"}');

        expect(new CollectionItem("movie", new MovieContentModel(1, "Desperately Seeking Susan", "//posters.epix.com/1/playerposter/200x114.jpg", "desperately-seeking-susan", 1985, "PG-13", 7839.57, {
            id: 11353,
            movie_id: 1,
            urls: {
                rottentomatoes: "http://www.rottentomatoes.com/m/desperately_seeking_susan",
                flixster: "http://www.flixster.com/movie/desperately-seeking-susan"
            },
            criticScore: {
                score: 87,
                url: "http://www.rottentomatoes.com/m/desperately_seeking_susan#reviews",
                cerifiedFresh: false,
                rotten: false
            },
            fanScore: {score: 62, url: "http://www.rottentomatoes.com/m/desperately_seeking_susan/reviews/?type=user"}
        }, "//posters.epix.com/1/playerposter.jpg"), 'navigate', 'default')).toEqual(CollectionItem.fromJSON(jsonObject));
    })
});