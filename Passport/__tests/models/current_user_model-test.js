jest.unmock('../../src/models/current_user_model.js');

import {CurrentUserModel} from "../../src/models/current_user_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"queued": true, "queue_item_id":1}');

        expect(new CurrentUserModel(true, 1)).toEqual(CurrentUserModel.fromJSON(jsonObject));
    })
});