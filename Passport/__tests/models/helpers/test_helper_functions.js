export function jsonCanBeParsedTest(json_string, model_instance, model_class) {
    describe('json', () => {
        it('can be parsed', () => {
            const jsonObject = JSON.parse(json_string);

            expect(model_instance).toEqual(model_class.fromJSON(jsonObject));
        })
    });
}

export function idIsRequiredTest(model_class) {
    describe('attribute id', () => {
        it('is required', () => {
            expect(function () {
                const testVar = new model_class(null);
            }).toThrow(new Error('id is required!'));
        })
    });
}