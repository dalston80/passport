import {GenresModel} from '../../../src/models/genres_model';
import {GenreItem} from '../../../src/models/genre_item';
import {CollectionContentModel} from "../../../src/models/collection_content_model";
import {CollectionItem} from "../../../src/models/collection_item";
import {MovieListItem} from "../../../src/models/movie_list_item";
import {ChannelsModel} from '../../../src/models/channels_model';
import {StuntsModel} from '../../../src/models/stunts_model';

export const genresTestVars = {
    genresJson: '{"genre":{' +
    '"meta": {"total_count": 2},' +
    ' "items": [{"type":"genre", "content":{"id":1, "title":"My Genre", "thumbnail":null}, "action":"navigate", "target":"default"}, {"type":"genre", "content":{"id":2, "title":"My Genre2", "thumbnail":null}, "action":"navigate", "target":"default"}]}}',
    genresInstance: new GenresModel({total_count: 2}, [new CollectionItem("genre", new CollectionContentModel(1, "My Genre", null), "navigate", "default"), new CollectionItem("genre", new CollectionContentModel(2, "My Genre2", null), "navigate", "default")]),
    genresClass: GenresModel
};

export const genreItemTestVars = {
    genreItemJson: '{"id":53, "name":"Recently Added", "movies":[{"id":855, "title":"Top Gun", "short_name":"top-gun", "release_year":1986, "mpaa_rating":"PG", "duration":6575.29, "genres":["Recently Added", "Action", "Drama"]}, {"id":13500, "title":"Capote", "short_name":"capote", "release_year":2005, "mpaa_rating":"R", "duration":6857.92, "genres":["Recently Added", "Biography", "Drama"]}]}',
    genreItemInstance: new GenreItem(53,
        'Recently Added',
        [new MovieListItem(855, "Top Gun", "PG", 1986, 6575.29, "top-gun", undefined, [
            "Recently Added",
            "Action",
            "Drama"
        ], undefined, undefined, undefined, undefined), new MovieListItem(13500, "Capote", "R", 2005, 6857.92, "capote", undefined, [
            "Recently Added",
            "Biography",
            "Drama"
        ], undefined, undefined, undefined, undefined)]),
    genreItemClass: GenreItem
};

export const channelTestVars = {
    channelJson: '{"name": "EPIX","short_name": "EPIX","path": "http://epixurtmp-lh.akamaihd.net/i/epix1hdn_1@101577/master.m3u8","logo": "http://content.epixhd.com/webassets/livechannels/epixlive1_poster_sd.jpg","timer": 2827, "now_playing":{"id": 14150,"title": "The Voices","short_name": "the-voices","release_year": 2015,"mpaa_rating": "R","duration": 6218.68,"genres": ["Comedy","Horror"]}, "up_next":{"id": 13700,"title": "The Quiet Ones","short_name": "the-quiet-ones","release_year": 2014,"mpaa_rating": "PG-13","duration": 5893.78,"genres": ["Horror"]}}',
    channelInstance: new ChannelsModel("EPIX", "EPIX", "http://epixurtmp-lh.akamaihd.net/i/epix1hdn_1@101577/master.m3u8", "http://content.epixhd.com/webassets/livechannels/epixlive1_poster_sd.jpg", 2827, new MovieListItem(14150, "The Voices", "R", 2015, 6218.68, "the-voices", undefined, ["Comedy", "Horror"], undefined, undefined, undefined, undefined), new MovieListItem(13700, "The Quiet Ones", "PG-13", 2014, 5893.78, "the-quiet-ones", undefined, ["Horror"], undefined, undefined, undefined, undefined)),
    channelClass: ChannelsModel
};