export const movieListItemResponse = {
    "id": 13601,
    "title": "The Hunger Games: Mockingjay - Part 1",
    "short_name": "the-hunger-games-mockingjay-part-1",
    "release_year": 2014,
    "mpaa_rating": "PG-13",
    "duration": 7370.71,
    "genres": [
        "Action",
        "Drama",
        "Sci-Fi and Fantasy"
    ]
};