jest.unmock('../../src/models/entitlements_model.js');
jest.unmock('../../src/models/posters_model.js');
jest.unmock('../../src/models/current_user_model.js');
jest.unmock('../../src/models/movie_list_item.js');
jest.unmock('../../src/helpers/arrays.js');

import {EntitlementsModel} from '../../src/models/entitlements_model';
import {PostersModel} from '../../src/models/posters_model';
import {CurrentUserModel} from '../../src/models/current_user_model';
import {MovieListItem} from '../../src/models/movie_list_item';


describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id": 13601,"title": "The Hunger Games: Mockingjay - Part 1","short_name": "the-hunger-games-mockingjay-part-1","release_year": 2014,"mpaa_rating": "PG-13","duration": 7370.71,"genres": ["Action","Drama","Sci-Fi and Fantasy"]}');

        expect(new MovieListItem(13601, "The Hunger Games: Mockingjay - Part 1", "PG-13", 2014, 7370.71, "the-hunger-games-mockingjay-part-1", undefined, ["Action", "Drama", "Sci-Fi and Fantasy"], undefined, undefined, undefined, undefined)).toEqual(MovieListItem.fromJSON(jsonObject));
    })
});