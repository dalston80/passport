jest.unmock('../../src/models/collection_content_model.js');
jest.unmock('../../src/helpers/arrays.js');

import {CollectionContentModel} from '../../src/models/collection_content_model.js';

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id":1, "title":"Title1", "thumbnail":null, "name":"Name1", "url":"www.google.com"}');

        expect(new CollectionContentModel(1, "Title1", null, "Name1", "www.google.com")).toEqual(CollectionContentModel.fromJSON(jsonObject));
    })
});