jest.unmock('../../src/models/movie_content_model.js');
jest.unmock('../../src/models/entitlements_model.js');
jest.unmock('../../src/helpers/arrays.js');

import {MovieContentModel} from '../../src/models/movie_content_model.js';
import {EntitlementsModel} from '../../src/models/entitlements_model.js';

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id": 1,"title": "Desperately Seeking Susan","thumbnail": "//posters.epix.com/1/playerposter/200x114.jpg","short_name": "desperately-seeking-susan","release_year": 1985,"mpaa_rating": "PG-13","duration": 7839.57,"rotten_tomatoes_review": {"id": 11353,"movie_id": 1,"urls": {"rottentomatoes":"http://www.rottentomatoes.com/m/desperately_seeking_susan","flixster":"http://www.flixster.com/movie/desperately-seeking-susan"},"criticScore": {"score":87,"url":"http://www.rottentomatoes.com/m/desperately_seeking_susan#reviews","certifiedFresh":false,"rotten":false},"fanScore": {"score":62,"url":"http://www.rottentomatoes.com/m/desperately_seeking_susan/reviews/?type=user"}},"background": "//posters.epix.com/1/playerposter.jpg", "banner":"https://dl.dropboxusercontent.com/u/75847465/flag-free.png", "entitlements":{"playable": false,"ovx_enabled": false,"start_date": null,"end_date": null,"service_level": null,"parental_control_restricted": false,"play_notice": "This movie is not currently playing on EPIX","coming_soon": null,"ending_soon": null}}');

        expect(new MovieContentModel(1, "Desperately Seeking Susan", "//posters.epix.com/1/playerposter/200x114.jpg", "desperately-seeking-susan", 1985, "PG-13", 7839.57, {
            id: 11353,
            movie_id: 1,
            urls: {
                rottentomatoes: "http://www.rottentomatoes.com/m/desperately_seeking_susan",
                flixster: "http://www.flixster.com/movie/desperately-seeking-susan"
            },
            criticScore: {
                score: 87,
                url: "http://www.rottentomatoes.com/m/desperately_seeking_susan#reviews",
                certifiedFresh: false,
                rotten: false
            },
            fanScore: {
                score: 62,
                url: "http://www.rottentomatoes.com/m/desperately_seeking_susan/reviews/?type=user"
            }
            }, "//posters.epix.com/1/playerposter.jpg", "https://dl.dropboxusercontent.com/u/75847465/flag-free.png",
            {
                playable: false,
                ovx_enabled: false,
                start_date: null,
                end_date: null,
                service_level: null,
                parental_control_restricted: false,
                play_notice: "This movie is not currently playing on EPIX",
                coming_soon: null,
                ending_soon: null
            }
        )).toEqual(MovieContentModel.fromJSON(jsonObject));
    })
});