jest.unmock('../../src/models/extras_item.js');

import {ExtrasItem} from "../../src/models/extras_item";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id":1, "title":"My Title", "duration":1000, "description":"My Description", "hlspath":"/hlspath.mp4", "poster":"/poster.png", "movie_id":1, "item":{"item_type":"video"}}');

        expect(new ExtrasItem(1, 'My Title', 1000, 'My Description', '/hlspath.mp4', '/poster.png', 1, {item_type: "video"})).toEqual(ExtrasItem.fromJSON(jsonObject));
    })
});