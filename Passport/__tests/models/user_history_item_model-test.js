jest.unmock('../../src/models/user_history_item_model.js');

import {UserHistoryItemModel} from "../../src/models/user_history_item_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id":1, "created_at":"1999-01-01T00:00:00.000Z", "user_id":234, "item_id":50, "item_type":"movie"}');

        expect(new UserHistoryItemModel(1, "1999-01-01T00:00:00.000Z", 234, 50, "movie")).toEqual(UserHistoryItemModel.fromJSON(jsonObject));
    })
});