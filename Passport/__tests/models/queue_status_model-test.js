jest.unmock('../../src/models/queue_status_model.js');

import {QueueStatusModel} from "../../src/models/queue_status_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"movie":{"queued": true, "queue_item_id":12}}');

        expect(new QueueStatusModel(true, 12)).toEqual(QueueStatusModel.fromJSON(jsonObject));
    })
});