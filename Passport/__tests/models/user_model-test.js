jest.unmock('../../src/models/user_model.js');
jest.unmock('../../src/helpers/arrays.js');

import {UserModel} from '../../src/models/user_model.js';

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"user_session":{"user_id":1, "mso_name":"Verizon", "msologos":{"small":"smallthumb.png", "medium":"mediumthumb.png", "large":"largethumb.png"}, "service_level":"EAU", "attributes":{"FREE_TRIAL_TYPE":"NONE"}, "ovx_enabled":false, "entitled":false, "entitlement_status":"unentitled"}}');

        expect(new UserModel(1, "Verizon", {
            small: "smallthumb.png",
            medium: "mediumthumb.png",
            large: "largethumb.png"
        }, "EAU", {FREE_TRIAL_TYPE: "NONE"}, false, false, "unentitled")).toEqual(UserModel.fromJSON(jsonObject));
    })
});