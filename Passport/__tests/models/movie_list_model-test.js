jest.unmock('../../src/models/movie_list_model.js');
jest.unmock('../../src/models/movie_list_item.js');
jest.unmock('../../src/helpers/arrays.js');

import {MovieListModel} from "../../src/models/movie_list_model";
import {MovieListItem} from "../../src/models/movie_list_item";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{' +
            '"meta": {"total_count": 2},' +
            ' "movies": [{"id": 13601,"title": "The Hunger Games: Mockingjay - Part 1","short_name": "the-hunger-games-mockingjay-part-1","release_year": 2014,"mpaa_rating": "PG-13","duration": 7370.71,"genres": ["Action","Drama","Sci-Fi and Fantasy"]}, {"id": 14746,"title": "Creed: The Legacy of a Legend","short_name": "creed-the-legacy-of-a-legend","release_year": 2016,"mpaa_rating": "NR","duration": 1715.86,"genres": ["Documentary"]}]}');

        expect(new MovieListModel({total_count: 2}, [new MovieListItem(13601, "The Hunger Games: Mockingjay - Part 1", "PG-13", 2014, 7370.71, "the-hunger-games-mockingjay-part-1", undefined, ["Action", "Drama", "Sci-Fi and Fantasy"], undefined, undefined, undefined, undefined), new MovieListItem(14746, "Creed: The Legacy of a Legend", "NR", 2016, 1715.86, "creed-the-legacy-of-a-legend", undefined, ["Documentary"], undefined, undefined, undefined, undefined)])).toEqual(MovieListModel.fromJSON(jsonObject));
    })
});