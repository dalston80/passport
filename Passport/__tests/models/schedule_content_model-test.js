jest.unmock('../../src/models/schedule_content_model.js');
jest.unmock('../../src/helpers/arrays.js');

import {ScheduleContentModel} from '../../src/models/schedule_content_model.js';

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id": 678572, "title": "The Vatican Tapes","thumbnail": "", "starts_at": "2016-04-30T04:15:00Z","rating": "PG-13"}');

        expect(new ScheduleContentModel(678572, "The Vatican Tapes", "", "2016-04-30T04:15:00Z", "PG-13")).toEqual(ScheduleContentModel.fromJSON(jsonObject));
    })
});