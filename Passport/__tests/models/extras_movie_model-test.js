jest.unmock('../../src/models/extras_movie_model.js');

import {ExtrasMovieModel} from "../../src/models/extras_movie_model";

describe('json', () => {
    it('can be parsed', () => {

        const jsonObject = JSON.parse('{"images": {"posters": [{"id": 434988,"bitly": null,"original_url": "//content.epixhd.com/webassets/static/movies/images/13601/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434988.jpg","websized_url": "//content.epixhd.com/webassets/static/movies/images/websized/13601/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434988.jpg","thumbnail_url": "//content.epixhd.com/webassets/static/movies/images/13601/thumbs/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434988.jpg"}]}, "videos":{"behindthescenes":[{"id": 9346,"bitly": null,"title": "On The Set Of The Hunger Games Mockingjay Part 1","duration": 94.19,"description": "Take a look behind the scenes with the cast and crew of The Hunger Games.","hlspath": "https://epixhlsxtrts-i.akamaihd.net/i/webroot/webassets/static/movies/videos/13601/The_Hunger_Games:_Mockingjay_-_Part_1_behindthescenes_9346.mp4/master.m3u8","url": "//content.epixhd.com/webassets/static/movies/videos/13601/The_Hunger_Games:_Mockingjay_-_Part_1_behindthescenes_9346.mp4","poster": {"id": 443527,"bitly": null,"original_url": "//content.epixhd.com/webassets/static/movies/images/13601/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_443527.jpg","websized_url": "//content.epixhd.com/webassets/static/movies/images/websized/13601/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_443527.jpg","thumbnail_url": "//content.epixhd.com/webassets/static/movies/images/13601/thumbs/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_443527.jpg"}}]}}');

        expect(new ExtrasMovieModel({
            posters: [
                {
                    id: 434988,
                    bitly: null,
                    original_url: "//content.epixhd.com/webassets/static/movies/images/13601/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434988.jpg",
                    websized_url: "//content.epixhd.com/webassets/static/movies/images/websized/13601/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434988.jpg",
                    thumbnail_url: "//content.epixhd.com/webassets/static/movies/images/13601/thumbs/The_Hunger_Games:_Mockingjay_-_Part_1_posters_434988.jpg"
                }
            ]
        }, {
            behindthescenes: [
                {
                    id: 9346,
                    bitly: null,
                    title: "On The Set Of The Hunger Games Mockingjay Part 1",
                    duration: 94.19,
                    description: "Take a look behind the scenes with the cast and crew of The Hunger Games.",
                    hlspath: "https://epixhlsxtrts-i.akamaihd.net/i/webroot/webassets/static/movies/videos/13601/The_Hunger_Games:_Mockingjay_-_Part_1_behindthescenes_9346.mp4/master.m3u8",
                    url: "//content.epixhd.com/webassets/static/movies/videos/13601/The_Hunger_Games:_Mockingjay_-_Part_1_behindthescenes_9346.mp4",
                    poster: {
                        id: 443527,
                        bitly: null,
                        original_url: "//content.epixhd.com/webassets/static/movies/images/13601/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_443527.jpg",
                        websized_url: "//content.epixhd.com/webassets/static/movies/images/websized/13601/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_443527.jpg",
                        thumbnail_url: "//content.epixhd.com/webassets/static/movies/images/13601/thumbs/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_443527.jpg"
                    }
                }
            ]
        })).toEqual(ExtrasMovieModel.fromJSON(jsonObject));
    })
});