jest.unmock('../../src/models/entitlements_model.js');

import {EntitlementsModel} from "../../src/models/entitlements_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"playable":false, "ovx_enabled":false, "parental_control_restricted":false, "start_date":"start date", "end_date":"end date", "service_level":"EEE", "play_notice":"movie not playable"}');

        expect(new EntitlementsModel(false, false, false, "start date", "end date", "EEE", "movie not playable")).toEqual(EntitlementsModel.fromJSON(jsonObject));
    })
});