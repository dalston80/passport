jest.unmock('../../src/models/stunt_model.js');
jest.unmock('../../src/models/stunt_item.js');
jest.unmock('../../src/models/movie_list_item.js');
jest.unmock('../../src/helpers/arrays.js');

import {StuntModel} from "../../src/models/stunt_model";
import {StuntItem} from "../../src/models/stunt_item";
import {MovieListItem} from "../../src/models/movie_list_item";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{' +
            '"meta": {"total_count": 1},' +
            ' "stunt": {"id":1, "name":"Most Watched", "movies":[{"id":13428,"title":"P!NK: The Truth About Love Tour","short_name":"pink-the-truth-about-love-tour","release_year":2013,"mpaa_rating":"TV-14","duration":6629.81,"genres":["Concert", "EPIX Originals"]}]}}');

        expect(new StuntModel({total_count: 1}, new StuntItem(1, 'Most Watched', [new MovieListItem(13428, "P!NK: The Truth About Love Tour", "TV-14", 2013, 6629.81, "pink-the-truth-about-love-tour", undefined, ["Concert", "EPIX Originals"], undefined, undefined, undefined, undefined)]))).toEqual(StuntModel.fromJSON(jsonObject));
    })
});