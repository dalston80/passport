jest.unmock('../../src/models/movie_play_model.js');

import {MoviePlayModel} from "../../src/models/movie_play_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id": 5099,"position": 0,"playlist":[{"type": "poster","path": "http://content.epixhd.com/webassets/static/movies/images/5099/The_Hunger_Games:_Catching_Fire_playerposters_434014.jpg"},{"type": "video","path": "https://epixvodchromcast-vh.akamaihd.net/i/epixcont1/NTAtest/ids/id_5_second_alternate,,.mp4.csmil/master.m3u8?hdnea=st%3D1430089730%7Eexp%3D1430090030%7Eacl%3D%2F%2A%7Ehmac%3D04fce66f39304e47b0b68e98bdea010c7a0b61d0db20111108856415f0b98a47"}], "expires_at":"2015-06-05T11:37:01.000Z", "entitlements": {"playable": true,"ovx_enabled": false,"start_date": "2014-07-26T00:00:00.000Z","end_date": "2016-04-21T23:59:59.000Z","service_level": "EA0","parental_control_restricted": false,"play_notice": null,"coming_soon": null,"ending_soon": null}}');

        expect(new MoviePlayModel(
            5099,
            0,
            [
                {
                    type: "poster",
                    path: "http://content.epixhd.com/webassets/static/movies/images/5099/The_Hunger_Games:_Catching_Fire_playerposters_434014.jpg"
                },
                {
                    type: "video",
                    path: "https://epixvodchromcast-vh.akamaihd.net/i/epixcont1/NTAtest/ids/id_5_second_alternate,,.mp4.csmil/master.m3u8?hdnea=st%3D1430089730%7Eexp%3D1430090030%7Eacl%3D%2F%2A%7Ehmac%3D04fce66f39304e47b0b68e98bdea010c7a0b61d0db20111108856415f0b98a47"
                }
            ],
            "2015-06-05T11:37:01.000Z",
            {
                playable: true,
                ovx_enabled: false,
                start_date: "2014-07-26T00:00:00.000Z",
                end_date: "2016-04-21T23:59:59.000Z",
                service_level: "EA0",
                parental_control_restricted: false,
                play_notice: null,
                coming_soon: null,
                ending_soon: null
            }
        )).toEqual(MoviePlayModel.fromJSON(jsonObject));
    })
});