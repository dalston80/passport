jest.unmock('../../src/models/collections_model.js');

import {CollectionsModel} from "../../src/models/collections_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{' +
            '"meta": {"total_count": 1},' +
            ' "collections": [{"id":1, "name":"My Name", "description":"My Description", "url":"My URL", "tags":"My Tags"}]}');

        expect(new CollectionsModel({total_count: 1}, [{
            id: 1,
            name: "My Name",
            description: "My Description",
            url: "My URL",
            tags: "My Tags"
        }])).toEqual(CollectionsModel.fromJSON(jsonObject));
    })
});