jest.unmock('../../src/models/channels_model.js');
jest.unmock('../../src/models/movie_list_item.js');
jest.unmock('../../src/helpers/arrays.js');

import {jsonCanBeParsedTest} from './helpers/test_helper_functions';
import {channelTestVars} from './helpers/test_helper_variables';

jsonCanBeParsedTest(channelTestVars.channelJson, channelTestVars.channelInstance, channelTestVars.channelClass);