jest.unmock('../../src/models/live_feed_model.js');
jest.unmock('../../src/models/channels_model.js');
jest.unmock('../../src/helpers/arrays.js');

import {LiveFeedModel} from '../../src/models/live_feed_model.js';
import {ChannelsModel} from '../../src/models/channels_model.js';

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"channels":[{"name": "EPIX","short_name": "EPIX","path": "http://epixurtmp-lh.akamaihd.net/i/epix1hdn_1@101577/master.m3u8","logo": "http://content.epixhd.com/webassets/livechannels/epixlive1_poster_sd.jpg","timer": 2827}]}');

        expect(new LiveFeedModel([new ChannelsModel("EPIX", "EPIX", "http://epixurtmp-lh.akamaihd.net/i/epix1hdn_1@101577/master.m3u8", "http://content.epixhd.com/webassets/livechannels/epixlive1_poster_sd.jpg", 2827, undefined, undefined)])).toEqual(LiveFeedModel.fromJSON(jsonObject));
    })
});