jest.unmock('../../src/models/user_queue_model.js');
jest.unmock('../../src/models/queue_collection_item_model.js');
jest.unmock('../../src/models/movie_content_model.js');
jest.unmock('../../src/helpers/content_model_selector');
jest.unmock('../../src/helpers/arrays.js');

import {UserQueueModel} from "../../src/models/user_queue_model";
import {QueueCollectionItemModel} from "../../src/models/queue_collection_item_model.js";
import {MovieContentModel} from "../../src/models/movie_content_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"queue": {' +
            '"meta": {"total_count": 1},' +
            ' "items": [{"type":"movie", "content":{"id": 1,"title": "Desperately Seeking Susan","thumbnail": "//posters.epix.com/1/playerposter/200x114.jpg","short_name": "desperately-seeking-susan","release_year": 1985,"mpaa_rating": "PG-13","duration": 7839.57,"rotten_tomatoes_review": null,"background": "//posters.epix.com/1/playerposter.jpg"}, "action":"navigate", "target":"default", "id":1, "ordering":1}]}}');

        expect(new UserQueueModel({total_count: 1}, [new QueueCollectionItemModel("movie", new MovieContentModel(1, "Desperately Seeking Susan", "//posters.epix.com/1/playerposter/200x114.jpg", "desperately-seeking-susan", 1985, "PG-13", 7839.57, null, "//posters.epix.com/1/playerposter.jpg"), "navigate", "default", 1, 1)])).toEqual(UserQueueModel.fromJSON(jsonObject));
    })
});

describe('empty queue', () => {
    it('has a list with no queue items', () => {
        const queue = new UserQueueModel(null, null);
        expect(queue.items.length).toEqual(0);
    })
});