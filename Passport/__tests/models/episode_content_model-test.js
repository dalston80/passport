jest.unmock('../../src/models/episode_content_model.js');
jest.unmock('../../src/helpers/arrays.js');

import {EpisodeContentModel} from '../../src/models/episode_content_model.js';

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id": 1, "title": "EPIX Presents Road to the NHL Winter Classic", "thumbnail": "//posters.epix.com/14336/playerposter/200x114.jpg","short_name": "road-to-the-winter-classic-1-2015","release_year": 2015,"mpaa_rating": "TV-MA","duration": 3507.7,"rotten_tomatoes_review": null,"background": "//posters.epix.com/14336/playerposter.jpg","next_episode": {"id": 2,"title": "EPIX Presents Road to the NHL Winter Classic Episode 2","thumbnail": "//posters.epix.com/14340/playerposter/200x114.jpg"}, "season_id":999}');

        expect(new EpisodeContentModel(1, "EPIX Presents Road to the NHL Winter Classic", "//posters.epix.com/14336/playerposter/200x114.jpg", "road-to-the-winter-classic-1-2015", 2015, "TV-MA", 3507.7, null, "//posters.epix.com/14336/playerposter.jpg", {
            id: 2,
            title: "EPIX Presents Road to the NHL Winter Classic Episode 2",
            thumbnail: "//posters.epix.com/14340/playerposter/200x114.jpg"
        }, 999)).toEqual(EpisodeContentModel.fromJSON(jsonObject));
    })
});