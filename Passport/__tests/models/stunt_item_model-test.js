jest.unmock('../../src/models/stunt_item.js');
jest.unmock('../../src/models/movie_list_item');
jest.unmock('../../src/helpers/arrays.js');

import {StuntItem} from "../../src/models/stunt_item";
import {MovieListItem} from '../../src/models/movie_list_item';

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id": 1, "name": "Most Watched", "movies": [{"id": 13428, "title": "P!NK: The Truth About Love Tour", "short_name": "pink-the-truth-about-love-tour", "release_year": 2013, "mpaa_rating": "TV-14", "duration": 6629.81, "genres": ["Concert","EPIX Originals"]}, {"id": 13415, "title": "Schooled: The Price of College Sports", "short_name": "schooled-the-price-of-college-sports", "release_year": 2013, "mpaa_rating": "TV-PG", "duration": 4849.67, "genres": ["Documentary","EPIX Originals","Sports Films"]}]}');

        expect(new StuntItem(1,
            'Most Watched',
            [new MovieListItem(13428, "P!NK: The Truth About Love Tour", "TV-14", 2013, 6629.81, "pink-the-truth-about-love-tour", undefined, [
                "Concert",
                "EPIX Originals"
            ], undefined, undefined, undefined, undefined), new MovieListItem(13415, "Schooled: The Price of College Sports", "TV-PG", 2013, 4849.67, "schooled-the-price-of-college-sports", undefined, [
                "Documentary",
                "EPIX Originals",
                "Sports Films"
            ], undefined, undefined, undefined, undefined)])).toEqual(StuntItem.fromJSON(jsonObject));
    })
});