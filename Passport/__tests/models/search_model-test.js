jest.unmock('../../src/models/search_model.js');
jest.unmock('../../src/models/search_collection_item_model.js');
jest.unmock('../../src/models/movie_content_model.js');
jest.unmock('../../src/helpers/arrays.js');

import {SearchModel} from "../../src/models/search_model";
import {MovieContentModel} from "../../src/models/movie_content_model";
import {SearchCollectionItemModel} from "../../src/models/search_collection_item_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"search":{' +
            '"meta": {"total_count": 2},' +
            ' "items": [{"type":"movie", "content":{"id": 13601,"title": "The Hunger Games: Mockingjay - Part 1","short_name": "the-hunger-games-mockingjay-part-1","release_year": 2014,"mpaa_rating": "PG-13","duration": 7370.71, "rotten_tomatoes_review":null, "background":null}, "action":"navigate", "target":"default", "current_user":null}, {"type":"movie", "content":{"id": 14746,"title": "Creed: The Legacy of a Legend","short_name": "creed-the-legacy-of-a-legend","release_year": 2016,"mpaa_rating": "NR","duration": 1715.86, "rotten_tomatoes_review":null, "background":null}, "action":"navigate", "target":"default", "current_user":null}]}}');

        expect(new SearchModel({total_count: 2}, [new SearchCollectionItemModel("movie", new MovieContentModel(13601, "The Hunger Games: Mockingjay - Part 1", "PG-13", 2014, 7370.71, "the-hunger-games-mockingjay-part-1", null, null), "navigate", "default", null), new SearchCollectionItemModel("movie", new MovieContentModel(14746, "Creed: The Legacy of a Legend", "NR", 2016, 1715.86, "creed-the-legacy-of-a-legend", null, null), "navigate", "default", null)])).toEqual(SearchModel.fromJSON(jsonObject));
    })
});