jest.unmock('../../src/models/series_model.js');
jest.unmock('../../src/models/collection_item.js');
jest.unmock('../../src/models/collection_content_model.js');
jest.unmock('../../src/models/episode_content_model.js');
jest.unmock('../../src/helpers/content_model_selector.js');
jest.unmock('../../src/helpers/arrays.js');

import {SeriesModel} from "../../src/models/series_model";
import {CollectionContentModel} from "../../src/models/collection_content_model";
import {EpisodeContentModel} from "../../src/models/episode_content_model";
import {CollectionItem} from "../../src/models/collection_item";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"series":{"id":999, "title":"The Road to NHL", "description":null, "external_url":null, "next_episode":null, "seasons":[{"type":"season","content":{"id":998,"title":"Season 1","thumbnail":""},"action":"navigate","target":"default"}], "episodes":[{"type":"episode","content":{"id":992,"title":"Road To The NHL Winter Classic Episode 1","thumbnail":"//posters.epix.com/13716/playerposter/200x114.jpg","short_name":"epix-presents-road-to-the-nhl-winter-classic-episode-1","release_year":2014,"mpaa_rating":"TV-MA","duration":3398.52,"rotten_tomatoes_review":null,"background":"//posters.epix.com/13716/playerposter.jpg","next_episode":null},"action":"navigate","target":"default"}]}}');

        expect(new SeriesModel(999, "The Road to NHL", null, null, undefined, [new CollectionItem("season", new CollectionContentModel(998, "Season 1", ""), "navigate", "default")], [new CollectionItem("episode", new EpisodeContentModel(992, "Road To The NHL Winter Classic Episode 1", "//posters.epix.com/13716/playerposter/200x114.jpg", "epix-presents-road-to-the-nhl-winter-classic-episode-1", 2014, "TV-MA", 3398.52, null, "//posters.epix.com/13716/playerposter.jpg", null), "navigate", "default")])).toEqual(SeriesModel.fromJSON(jsonObject));
    })
});