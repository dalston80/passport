jest.unmock('../../src/models/episode_model.js');
jest.unmock('../../src/models/collection_content_model.js');

import {EpisodeModel} from "../../src/models/episode_model";
import {CollectionContentModel} from "../../src/models/collection_content_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"episode":{"id":999, "description":null, "next_episode":null, "movie":null, "season":{"id":999, "title":"Season 2", "thumbnail":""}, "series":{"id":999, "title":"The Road to NHL", "thumbnail":""}}}');

        expect(new EpisodeModel(999, null, undefined, undefined, new CollectionContentModel(999, "Season 2", ""), new CollectionContentModel(999, "The Road to NHL", ""))).toEqual(EpisodeModel.fromJSON(jsonObject));
    })
});