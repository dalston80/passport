jest.unmock('../../src/models/extras_model.js');

import {ExtrasModel} from "../../src/models/extras_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"meta":{"total_count":2}, "collections":[{"id":2,"title":"Extras Exclusive Content","description":"This is on the extras page.","display_image":null,"tags":null},{"id":3,"title":"Epix Exclusives","description":"For the Home page, Exclusive Content","display_image":null,"tags":null}]}');

        expect(new ExtrasModel({total_count: 2}, [{
            id: 2,
            title: "Extras Exclusive Content",
            description: "This is on the extras page.",
            display_image: null,
            tags: null
        }, {
            id: 3,
            title: "Epix Exclusives",
            description: "For the Home page, Exclusive Content",
            display_image: null,
            tags: null
        }])).toEqual(ExtrasModel.fromJSON(jsonObject));
    })
});