jest.unmock('../../src/models/video_item.js');

import {VideoItem} from "../../src/models/video_item";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id": 8961,"title": "My Title", "duration": 71.6, "description": "President Snow addresses the people of Panem in his address.", "hlspath": "https://epixhlsxtrts-i.akamaihd.net/i/webroot/webassets/static/movies/videos/13601/The_Hunger_Games:_Mockingjay_-_Part_1_trailers_8961.mp4/master.m3u8", "poster": {"id": 434488, "bitly": null, "original_url": "//content.epixhd.com/webassets/static/movies/images/13601/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_434488.jpg", "websized_url": "//content.epixhd.com/webassets/static/movies/images/websized/13601/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_434488.jpg", "thumbnail_url": "//content.epixhd.com/webassets/static/movies/images/13601/thumbs/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_434488.jpg"}}');

        expect(new VideoItem(
            8961,
            "My Title",
            71.6,
            "President Snow addresses the people of Panem in his address.",
            "https://epixhlsxtrts-i.akamaihd.net/i/webroot/webassets/static/movies/videos/13601/The_Hunger_Games:_Mockingjay_-_Part_1_trailers_8961.mp4/master.m3u8",
            {
                id: 434488,
                bitly: null,
                original_url: "//content.epixhd.com/webassets/static/movies/images/13601/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_434488.jpg",
                websized_url: "//content.epixhd.com/webassets/static/movies/images/websized/13601/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_434488.jpg",
                thumbnail_url: "//content.epixhd.com/webassets/static/movies/images/13601/thumbs/The_Hunger_Games:_Mockingjay_-_Part_1_videoposters_434488.jpg"
            }
        )).toEqual(VideoItem.fromJSON(jsonObject));
    })
});