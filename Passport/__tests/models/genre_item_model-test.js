jest.unmock('../../src/models/genre_item.js');
jest.unmock('../../src/models/movie_list_item.js');
jest.unmock('./helpers/test_helper_functions.js');
jest.unmock('./helpers/test_helper_variables.js');
jest.unmock('../../src/helpers/arrays.js');

import {GenreItem} from "../../src/models/genre_item";
import {MovieListItem} from '../../src/models/movie_list_item';
import {jsonCanBeParsedTest, idIsRequiredTest} from './helpers/test_helper_functions';
import {genreItemTestVars} from './helpers/test_helper_variables';

jsonCanBeParsedTest(genreItemTestVars.genreItemJson, genreItemTestVars.genreItemInstance, genreItemTestVars.genreItemClass);

idIsRequiredTest(genreItemTestVars.genreItemClass);

describe('empty list', () => {
    it('has a no content', () => {
        const genreItem = new GenreItem(1, null, null);
        expect(genreItem.movies.length).toEqual(0);
    })
});