jest.unmock('../../src/models/entitlements_model.js');
jest.unmock('../../src/models/posters_model.js');
jest.unmock('../../src/models/current_user_model.js');
jest.unmock('../../src/models/credits_model.js');
jest.unmock('../../src/models/video_item.js');
jest.unmock('../../src/models/extras_movie_model.js');
jest.unmock('../../src/models/movie_model.js');

import {EntitlementsModel} from '../../src/models/entitlements_model';
import {PostersModel} from '../../src/models/posters_model';
import {CurrentUserModel} from '../../src/models/current_user_model';
import {MovieModel} from '../../src/models/movie_model';
import {CreditsModel} from "../../src/models/credits_model";
import {ExtrasMovieModel} from "../../src/models/extras_movie_model";
import {VideoItem} from "../../src/models/video_item";


describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"movie": {"id": 13601,"title": "The Hunger Games: Mockingjay - Part 1","short_name": "the-hunger-games-mockingjay-part-1","release_year": 2014,"mpaa_rating": "PG-13","duration": 7370.71,"genres": ["Action","Drama","Sci-Fi and Fantasy"],"synopsis":"After destroying the Arena, Katniss Everdeen (Jennifer Lawrence) finds herself among unlikely allies: the residents of the mysterious District 13. Guided by President Coin (Julianne Moore) and rogue gamemaker Plutarch Heavensbee (Philip Seymour Hoffman), Katniss must decide whether or not she\'s ready to become a fierce symbol of revolution."}}');

        expect(new MovieModel(13601, "The Hunger Games: Mockingjay - Part 1", "PG-13", 2014, 7370.71, "the-hunger-games-mockingjay-part-1", ["Action", "Drama", "Sci-Fi and Fantasy"], undefined, undefined, undefined, "After destroying the Arena, Katniss Everdeen (Jennifer Lawrence) finds herself among unlikely allies: the residents of the mysterious District 13. Guided by President Coin (Julianne Moore) and rogue gamemaker Plutarch Heavensbee (Philip Seymour Hoffman), Katniss must decide whether or not she's ready to become a fierce symbol of revolution.", undefined, undefined, undefined, undefined)).toEqual(MovieModel.fromJSON(jsonObject));
    })
});