jest.unmock('../../src/models/user_queue_item_model.js');

import {UserQueueItemModel} from "../../src/models/user_queue_item_model";

describe('json', () => {
    it('can be parsed', () => {
        const jsonObject = JSON.parse('{"id":1, "movie_id":5099, "ordering":1, "created_at":"1999-01-01T00:00:00.000Z"}');

        expect(new UserQueueItemModel(1, 5099, 1, "1999-01-01T00:00:00.000Z")).toEqual(UserQueueItemModel.fromJSON(jsonObject));
    })
});

describe('attribute id', () => {
    it('is required', () => {
        expect(function () {
            const userQueueItem = new UserQueueItemModel(null, null, null, null);
        }).toThrow(new Error('id is required!'));
    })
});