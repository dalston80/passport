![epix_logo] (http://content.epixhd.com/styleassets/movie-pages/logo-white.png)

# EPIX JS Core Library: Passport

[![Circle CI](https://circleci.com/gh/Studio3/Passport.svg?style=svg)](https://circleci.com/gh/Studio3/Passport)
[![Test Coverage](https://codeclimate.com/repos/569813fffbdfc71316001e8c/badges/b8333b13070c668fe0e5/coverage.svg)](https://codeclimate.com/repos/569813fffbdfc71316001e8c/coverage)
[![Code Climate](https://codeclimate.com/repos/569813fffbdfc71316001e8c/badges/b8333b13070c668fe0e5/gpa.svg)](https://codeclimate.com/repos/569813fffbdfc71316001e8c/feed)
Core library for consuming the EPIX Mauka API

**Passport** is a solid and reliable JS client library for building experiences using the EPIX Mauka API. The goal
of **Passport** is to showcase how the services layer, in a typical application experience, can be implemented in a
Mauka client in order to build reliable and best of experience implementations. In other words: _This is a reference
implementation._

## Getting Started

Yippie! - In order to get started developing on Passport you need to go through a few steps

## Develop on Mac OS X El Capitan

### El Capitan Dependencies

1. Xcode
2. git
3. Homebrew
4. npm

#### Xcode

You only need the Xcode Command Line Tools.

The command line tools are `gcc`, `git` and `make`. Check if the full Xcode package is already installed:

```shell
$ xcode-select -p
```

If you see:

```
/Applications/Xcode.app/Contents/Developer
```

the full Xcode package is already installed. Use the Mac App Store application to find and install Xcode.
Immediately upgrade to the latest version using the updates section of the App Store.

Get Xcode command line tools.  Open terminal and execute the command `xcode-select --install`

#### Git

Check that git is installed:

`$ git version`

should produce:

```shell
$ git version 1.9.3 (Apple Git-50)
```
higher versions are ok, git is backward compatible.

Configure Git if you haven’t used it before. First, list the current settings with the `$ git config -l --global` command. Then set `user.name` and user.email if necessary:

```shell
$ git config -l --global
```

```
fatal: unable to read config file '/Users/.../.gitconfig': No such file or directory
```

```shell
$ git config --global user.name "Your Real Name"
```

```shell
$ git config --global user.email me@example.com
```

```shell
$ git config -l --global
```

```
user.name=Your Real Name
```

```
user.email=me@example.com
```

```
git config --local merge.ff only
```

Now you should be ready to collaborate with peers through GIT

#### Homebrew

```shell
$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
```

Then you should see:

```
===> Installation successful!
You should run 'brew doctor' *before* you install anything.
```

Do what it says, run `$ brew doctor`

Then you should see:

```
Your system is ready to brew
```

#### Node
To install node.js on OSX 10.11 El Capitan  you can download a pre-compiled binary package which makes a nice and easy installation.

Head over to [http://nodejs.org/](http://nodejs.org/) and click the install button to download the latest package

## Getting the code

1. Fork Studio3/Passport to your personal GitHub account
2. Clone your forked repo to your local computer (`$ git clone <username>/Passport`). This will create a `Passport` directory, cd to it.
3. Add a remote for fetching upstream changes (`$ git remote add studio3 https://github.com/Studio3/Passport.git`)
4. Install NPM dependencies with npm (`$ npm install`)

### Starting the development enviroment
1. Simply start webpack by using `$ npm run development`
2. Load [http://localhost:8080/webpack-dev-server/index.html](http://localhost:8080/webpack-dev-server/index.html) in your browser
3. As you make changes to the source code changes will automatically be updated in your browser

### Developing Passport

Follow these guidelines when working on branches and tickets. The rationale is that we want to know which commits apply to which ticket, we want to keep branch names as close, if not identical to the applicable ticket and we want to make it as easy as possible to type in the branch names:

1. Always have a JIRA ticket for a branch. The exception at the moment (subject to change) would be if we’re deploying to an environment and discover we have a config change or some other quick fix to the branch.
2. The branch name should be the Jira ticket reference, in lowercase, e.g. dev-1234, not DEV-1234, using dashes (-) not underscores (_).
3. On submission for review, each commit message should be prefixed with the ticket number like so: `[dev-1234] Does this to the code`.
4. If you cycle between various branches for the same ticket, DO NOT use long branch names when asking for review. For example, only decorate the branch name for your own private fork use,  `dev-1234-with-changes-post-review` and DO NOT submit that branch name in a pull request. Rather, use the original branch name matching the ticket, e.g. dev-1234 to reflect the latest development of the feature.
5. If the branch covers multiple tickets, create a Jira ticket that encompasses the child tasks and use the parent ticket as the branch name. In other words, DO NOT use dev-1234-1235.

### Versioning

As **Passport** is a JavaScript library exposed as a module, semver 2.0.0 principles will apply to any changes made to the library.