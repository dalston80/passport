var webpack = require('webpack');
var path = require('path');

var UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
var env = process.env.WEBPACK_ENV;

var libraryName = 'passport';
var plugins = [], outputFile;

if (env === 'prod') {
    plugins.push(new UglifyJsPlugin({ minimize: true }));
    outputFile = libraryName + '.min.js';
} else {
    outputFile = libraryName + '.js';
}

var sourceFolder = path.resolve(__dirname, 'src');
var testFolder = path.resolve(__dirname, '__tests');

var config = {
    entry: path.resolve(sourceFolder, 'main.js'),
    devtool: 'source-map',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: outputFile,
        library: libraryName,
        libraryTarget: 'umd',
        umdNamedDefine: true
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                include: [
                    sourceFolder,
                    testFolder
                ]
            },
            {
                test: /(\.js)$/,
                loader: "eslint-loader",
                include: [
                    sourceFolder,
                    testFolder
                ]
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            }
        ],
        eslint: {
            configFile: '.eslintrc.yml'
        }
    },
    plugins: [
      new webpack.ProvidePlugin({
        'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
      })
    ],
    resolve: {
        root: sourceFolder,
        extensions: ['', '.js']
    }/*,
    node: {
        console: 'empty',
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
     }*/
};

module.exports = config;