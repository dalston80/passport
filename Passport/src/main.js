import {Passport} from "epx_passport";
import {Config} from "config";
import {Logger} from "logger";

Logger.setLevel();

const passport = new Passport(Config.EPIX_ACCESS_TOKEN);

passport.series.getSeries(999).then(
    function (data) {
        Logger.debug('get series');
        Logger.debug(data);
    }
);