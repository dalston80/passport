const DEFAULT_PAGE = 1;
const DEFAULT_PAGE_SIZE = 50;

class Pagination {

    static getPaginationString(page, page_size) {
        return `/page/${page}/per_page/${page_size}`;
    }
}

export {Pagination, DEFAULT_PAGE, DEFAULT_PAGE_SIZE};