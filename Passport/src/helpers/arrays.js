class Arrays {

    static fromJSONArray(clazz, data) {
        if (data) {
            return Array.from(data, (x) => clazz.fromJSON(x))
        } else {
            return undefined;
        }
    }
}

export {Arrays};