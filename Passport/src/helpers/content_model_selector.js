import {MovieContentModel} from '../models/movie_content_model';
import {ScheduleContentModel} from '../models/schedule_content_model';
import {EpisodeContentModel} from '../models/episode_content_model';
import {CollectionContentModel} from '../models/collection_content_model';

class ContentModelSelector {

    static getModel(type, obj) {
        let content = {};
        let model = {};

        const typeObj = {
            movie: 'movie',
            episode: 'episode',
            collection: 'collection',
            schedule: 'schedule',
            genre: 'genre',
            stunt: 'stunt',
            series: 'series',
            promo: 'promo',
            image: 'image',
            video: 'video',
            app_section: 'app_section',
            movieContentModel: MovieContentModel,
            scheduleContentModel: ScheduleContentModel,
            episodeContentModel: EpisodeContentModel,
            collectionContentModel: CollectionContentModel,
            genreContentModel: CollectionContentModel,
            stuntContentModel: CollectionContentModel,
            seriesContentModel: CollectionContentModel,
            promoContentModel: CollectionContentModel,
            imageContentModel: CollectionContentModel,
            videoContentModel: CollectionContentModel,
            app_sectionContentModel: CollectionContentModel
        };

        switch (type) {
            case typeObj[type]:
                model = typeObj[type + 'ContentModel'];
                content = model.fromJSON(obj);
                break;
            default:
                content = CollectionContentModel.fromJSON(obj);
                break;
        }

        return content;
    }
}

export {ContentModelSelector};