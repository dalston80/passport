const Config = {
    ANALYTICS : {
        GOOGLE_TAG_MANAGER: 'GTM-NTJXGG',
        FACEBOOK: 6021374041854
    },
    BASE_URL: 'https://beta-api.epix.com/v2/',
    EPIX_ACCESS_TOKEN: 'e30370cec9af467e349cdeca2ce73cac-web',
    SUPPORTED_DEVICES: {
        WEB: ['web', 'browser', 'sandbox'],
        ANDROID_PHONE: ['android', 'phone', 'Nexus 6'],
        ANDROID_TABLET: ['android', 'tablet', 'Nexus 9'],
        ANDROID_TV: ['android', 'tv', 'MXIII M82'],
        KINDLE_PHONE: ['android', 'phone', 'KF3422', 'Amazon'],
        IOS_PHONE: ['ios', 'phone', 'iPhone'],
        IOS_TABLET: ['ios', 'tablet', 'iPad'],
        XBOX_ONE: ['xbox', 'console', 'Xbox One']
    },
    DEFAULT_USER_EMAIL: 'qa-user1@epixhd.com',
    DEFAULT_USER_PASSWORD: 'Epix1234'
}

export {Config}