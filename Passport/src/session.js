import {Config} from 'config';

let self = {};
let passport_session = {};

class Session {

    constructor(apikey, device) {
        this.apikey = apikey;
        this.device = device;
        self = this;
    }

    getDeviceSession() {
        const params = JSON.stringify({apikey: this.apikey, device: this.device});
        const sessionRequest = new XMLHttpRequest();
        sessionRequest.addEventListener('load', sessionLoadComplete);
        sessionRequest.open('POST', Config.BASE_URL + 'sessions', false);
        sessionRequest.setRequestHeader("Content-type", "application/json");
        sessionRequest.setRequestHeader("Accept", "application/json");
        sessionRequest.send(params);

        function sessionLoadComplete(event) {
            passport_session = JSON.parse(sessionRequest.responseText);
        }

        return passport_session;
    }

    static getToken() {
        if (!passport_session.device_session.session_token) {
            Session.startNewSession();
        }
        return passport_session.device_session.session_token;
    }

    static getSession() {
        return passport_session;
    }

    static startNewSession() {
        passport_session = self.getDeviceSession();
    }
}

export {Session};