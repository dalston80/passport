import {Session} from '../session';
import {Logger} from '../logger';

require('whatwg-fetch');

class HttpRequest {

    constructor(sessionToken, requestObj) {
        this.sessionToken = sessionToken;
        this.url = requestObj.url;
        this.method = requestObj.method;
        this.retryCount = 0;
    }

    static checkStatus(response, expectedStatusCode) {
        if (response.status >= 400 || (expectedStatusCode && expectedStatusCode !== response.status)) {
            Logger.debug(`HttpRequest failed for ${response.url}, response received: ${response}`);
            throw Error(`HttpRequest failed. Expected: ${response.expectedStatusCode}, Got: ${response.status}. Response: ${response} `);
        } else {
            return response;
        }
    }

    requestInvoker(url, requestData) {
        const self = this;
        return fetch(url, requestData)
            .then(
                function (data) {
                    return data;
                }
            )
            .catch(
                function (response) {
                    Logger.error(response);
                    // TODO: This should only be done on idempotent request
                    if (self.retryCount < 1) {
                        self.retryCount++;
                        Session.startNewSession();
                        self.request();
                    } else {
                        throw new Error('There was a problem with the network, please try again later');
                    }
                }
            );
    }

    httpRequestPayload(sessionToken, jsonBody) {
        const requestData = {
            method: this.method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-Session-Token': sessionToken
            }
        };

        if (jsonBody !== null) {
            Object.assign(requestData, {body: jsonBody})
        }
        return this.requestInvoker(this.url, requestData);
    }

    request(payload) {
        return this.httpRequestPayload(Session.getToken() || this.sessionToken, payload);
    }
}

export {HttpRequest};