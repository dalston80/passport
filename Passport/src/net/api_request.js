import {Config} from '../config';
import {HttpRequest} from 'net/http_request.js';
import {Pagination} from 'helpers/pagination.js';
import {Session} from 'session';

class ApiRequest {

    static makeRequest(url, method, payload) {
        return new HttpRequest(Session.getToken(), {url: Config.BASE_URL + url, method: method}).request(payload);
    }

    static doPaginatedGetRequest(path, model, page, page_size, params) {
        path = path + Pagination.getPaginationString(page, page_size);
        if (params) {
            path += '?' + params;
        }

        return this.doGetRequest(path, model);
    }

    static doGetRequest(path, model, payload) {
        return ApiRequest.makeRequest(path, "get", payload)
            .then((data) => {
                HttpRequest.checkStatus(data, 200);
                return data.json();
            })
            .then(function (data) {
                return model.fromJSON(data);
            })
    }

    static doDeleteRequest(path, payload) {
        ApiRequest.doDeleteRequestWithPayload(path, payload);
    }

    static doDeleteRequestWithPayload(path, payload) {
        return ApiRequest.makeRequest(path, "delete", payload)
            .then((data) => {
                HttpRequest.checkStatus(data, 204);
                return data.json();
            })
            .then(function (success) {
                return success; //'success' will be true upon successful call to this endpoint.
            })
    }

    static doPatchRequest(path, payload) {
        return ApiRequest.makeRequest(path, "patch", payload)
            .then((data) => {
                HttpRequest.checkStatus(data);
                return data.json();
            })
            .then(function (success) {
                return success; //'success' will be true upon successful call to this endpoint.
            })
    }

    static doPostRequest(path, model, payload) {
        return ApiRequest.makeRequest(path, "post", payload)
            .then((data) => {
                HttpRequest.checkStatus(data);
                return data.json();
            })
            .then(function (data) {
                return model.fromJSON(data);
            })
    }
}

export {ApiRequest};