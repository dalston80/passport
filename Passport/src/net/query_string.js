class QueryString {

    static getVars() {
        const query = window.location.search.substring(1);
        const queryArray = query.split("&");
        const length = queryArray.length;
        const temp = [];
        const queryObj = {};

        for (let i = 0; i < length; i++) {
            temp[i] = queryArray[i].split('=');
            queryObj[temp[i][0]] = temp[i][1];
        }

        return queryObj;
    }
}

export {QueryString};