import {Session} from 'session';
import {CollectionController} from 'controllers/collection_controller';
import {MovieController} from 'controllers/movie_controller';
import {ExtrasController} from 'controllers/extras_controller';
import {StuntsController} from 'controllers/stunts_controller';
import {GenreController} from 'controllers/genre_controller';
import {QueueController} from 'controllers/queue_controller';
import {HistoryController} from 'controllers/history_controller';
import {SearchController} from 'controllers/search_controller';
import {ChannelsController} from 'controllers/channels_controller';
import {EpisodeController} from 'controllers/episode_controller';
import {SeriesController} from 'controllers/series_controller';
import {UserController} from 'controllers/user_controller';
import {AuthController} from 'controllers/auth_controller';

class Passport {

    constructor(access_token, device) {
        this.session = new Session(access_token, device);
        this.collections = new CollectionController();
        this.search = new SearchController();
        this.movies = new MovieController();
        this.extras = new ExtrasController();
        this.stunts = new StuntsController();
        this.genres = new GenreController();
        this.queue = new QueueController();
        this.history = new HistoryController();
        this.channels = new ChannelsController();
        this.episode = new EpisodeController();
        this.series = new SeriesController();
        this.user = new UserController();
        this.auth = new AuthController();
        Session.startNewSession();
    }
}

export {Passport};