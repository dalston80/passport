import {PlayerModel} from "models/player_model";

class PostersModel {

    constructor(thumb, small, medium, large, chromecast, player, hero, story_art) {
        this.thumb = thumb;
        this.small = small;
        this.medium = medium;
        this.large = large;
        this.chromecast = chromecast;
        this.player = player;
        this.hero = hero;
        this.story_art = story_art;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }
        const {thumb, small, medium, large, chromecast, hero, story_art} = data;
        const player = PlayerModel.fromJSON(data.player);

        return new PostersModel(thumb, small, medium, large, chromecast, player, hero, story_art);
    }
}

export {PostersModel};