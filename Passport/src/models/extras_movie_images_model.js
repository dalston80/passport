class ExtrasMovieImagesModel {

    constructor(id, bitly, original_url, websized_url, thumbnail_url) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.bitly = bitly;
        this.original_url = original_url;
        this.websized_url = websized_url;
        this.thumbnail_url = thumbnail_url;
    }

    static fromJSON(data) {
        const {id, bitly, original_url, websized_url, thumbnail_url} = data;

        return new ExtrasMovieImagesModel(id, bitly, original_url, websized_url, thumbnail_url);
    }
}

export {ExtrasMovieImagesModel};