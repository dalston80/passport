import {CollectionContentModel} from './collection_content_model';

class ScheduleContentModel extends CollectionContentModel {

    constructor(id, title, thumbnail, starts_at, rating) {
        super(id, title, thumbnail);
        this.starts_at = starts_at;
        this.rating = rating;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }
        const {id, title, thumbnail, starts_at, rating} = data;

        return new ScheduleContentModel(id, title, thumbnail, starts_at, rating);
    }
}

export {ScheduleContentModel};