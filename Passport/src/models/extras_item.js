import {VideoItem} from "models/video_item";

class ExtrasItem extends VideoItem {

    constructor(id, title, duration, description, hlspath, poster, movie_id, item) {
        super(id, title, duration, description, hlspath, poster);
        this.movie_id = movie_id;
        this.item = item;
    }

    static fromJSON(data) {
        const {id, title, duration, description, hlspath, poster, movie_id, item} = data;

        return new ExtrasItem(id, title, duration, description, hlspath, poster, movie_id, item);
    }
}

export {ExtrasItem};