import {Arrays} from "helpers/arrays";
import {StuntItem} from "models/stunt_item";
import {CollectionItem} from "./collection_item";

class StuntsModel {

    constructor(meta, items) {
        if (!items) {
            items = [];
        }
        this.meta = meta;
        this.items = items;
    }

    static fromJSON(data) {
        const {meta} = data.stunt;
        const items = Arrays.fromJSONArray(CollectionItem, data.stunt.items);

        return new StuntsModel(meta, items);
    }
}

export {StuntsModel};