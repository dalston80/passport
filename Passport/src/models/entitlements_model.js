class EntitlementsModel {

    constructor(playable, ovx_enabled, parental_control_restricted, start_date, end_date, service_level, play_notice) {
        this.playable = playable;
        this.ovx_enabled = ovx_enabled;
        this.parental_control_restricted = parental_control_restricted;
        this.start_date = start_date;
        this.end_date = end_date;
        this.service_level = service_level;
        this.play_notice = play_notice;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }
        const {playable, ovx_enabled, parental_control_restricted, start_date, end_date, service_level, play_notice} = data;

        return new EntitlementsModel(playable, ovx_enabled, parental_control_restricted, start_date, end_date, service_level, play_notice);
    }
}

export {EntitlementsModel};