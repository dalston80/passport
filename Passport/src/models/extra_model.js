import {Arrays} from "helpers/arrays";
import {ExtrasItem} from "models/extras_item";
import {MovieListItem} from "models/movie_list_item";

class ExtraModel {

    constructor(id, movies, items) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.movies = movies;
        this.items = items;
    }

    static fromJSON(data) {
        const {id} = data.collection;
        const items = Arrays.fromJSONArray(ExtrasItem, data.collection.items);
        //const movies = Arrays.fromJSONArray(MovieListItem, data.collection.movies);
        //TODO: Need to update movie response for this endpoint...Right now it contains an object with movie objects instead of an array
        const tempObj = {};
        for (const prop in data.collection.movies) {
            if (data.collection.movies) {
                tempObj[prop] = MovieListItem.fromJSON(data.collection.movies[prop]);
            }
        }
        const movies = tempObj;

        return new ExtraModel(id, movies, items);
    }
}

export {ExtraModel};