import {Arrays} from 'helpers/arrays';
import {HistoryCollectionItemModel} from 'models/history_collection_item_model';

class UserHistoryModel {

    constructor(meta, items) {
        this.meta = meta;
        this.items = items;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {meta} = data.history;
        const items = Arrays.fromJSONArray(HistoryCollectionItemModel, data.history.items);

        return new UserHistoryModel(meta, items);
    }
}

export {UserHistoryModel};