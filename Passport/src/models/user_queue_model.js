import {Arrays} from "helpers/arrays";
import {QueueCollectionItemModel} from "models/queue_collection_item_model";

class UserQueueModel {

    constructor(meta, items) {
        if (!items) {
            items = [];
        }
        this.meta = meta;
        this.items = items;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }
        const {meta} = data.queue;
        const items = Arrays.fromJSONArray(QueueCollectionItemModel, data.queue.items);

        return new UserQueueModel(meta, items);
    }
}

export {UserQueueModel};