class ExtrasModel {

    constructor(meta, collections) {
        this.meta = meta;
        this.collections = collections;
    }

    static fromJSON(data) {
        const {meta, collections} = data;

        return new ExtrasModel(meta, collections);
    }
}

export {ExtrasModel};