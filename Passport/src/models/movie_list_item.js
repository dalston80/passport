import {PostersModel} from "models/posters_model";
import {EntitlementsModel} from "models/entitlements_model";
import {CurrentUserModel} from "models/current_user_model";

class MovieListItem {

    constructor(id, title, mpaa_rating, release_year, duration, short_name, posters, genres, rotten_tomatoes_review, entitlements, ovx, current_user) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.title = title;
        this.mpaa_rating = mpaa_rating;
        this.release_year = release_year;
        this.duration = duration;
        this.short_name = short_name;
        this.posters = posters;
        this.genres = genres;
        this.rotten_tomatoes_review = rotten_tomatoes_review;
        this.entitlements = entitlements;
        this.ovx = ovx;
        this.current_user = current_user;
    }

    static fromJSON(data) {
        const {id, title, mpaa_rating, release_year, duration, short_name, genres, rotten_tomatoes_review} = data;
        const posters = PostersModel.fromJSON(data.posters);
        const entitlements = EntitlementsModel.fromJSON(data.entitlements);
        const current_user = CurrentUserModel.fromJSON(data.current_user);

        return new MovieListItem(id, title, mpaa_rating, release_year, duration, short_name, posters, genres, rotten_tomatoes_review, entitlements, current_user);
    }
}

export {MovieListItem};