class MoviePlayheadPositionModel {

    constructor(id, position) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.position = position;
    }

    static fromJSON(data) {
        const {id, position} = data.movie;

        return new MoviePlayheadPositionModel(id, position);
    }
}

export {MoviePlayheadPositionModel};