class CreditsModel {

    constructor(type, name, role) {
        this.type = type;
        this.name = name;
        this.role = role;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }
        const {type, name, role} = data;

        return new CreditsModel(type, name, role);
    }
}

export {CreditsModel};