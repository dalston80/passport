import {CollectionItem} from './collection_item';
import {ContentModelSelector} from '../helpers/content_model_selector';

class QueueCollectionItemModel extends CollectionItem {

    constructor(type, content, action, target, id, ordering) {
        super(type, content, action, target);

        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.ordering = ordering;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {type, action, target, id, ordering} = data;
        const content = ContentModelSelector.getModel(type, data.content);

        return new QueueCollectionItemModel(type, content, action, target, id, ordering);
    }
}

export {QueueCollectionItemModel};