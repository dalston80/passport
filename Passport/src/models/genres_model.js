import {Arrays} from "helpers/arrays";
import {GenreItem} from "./genre_item";
import {CollectionItem} from './collection_item'

class GenresModel {

    constructor(meta, items) {
        if (!items) {
            items = [];
        }
        this.meta = meta;
        this.items = items;
    }

    static fromJSON(data) {
        const {meta} = data.genre;
        const items = Arrays.fromJSONArray(CollectionItem, data.genre.items);

        return new GenresModel(meta, items);
    }
}

export {GenresModel};