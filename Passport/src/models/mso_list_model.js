class MSOListModel {

    constructor(msos) {
        this.msos = msos;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const msos = data.msos;

        return new MSOListModel(msos);
    }
}

export {MSOListModel};