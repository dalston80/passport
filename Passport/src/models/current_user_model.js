class CurrentUserModel {

    constructor(queued, queue_item_id) {
        this.queued = queued;
        this.queue_item_id = queue_item_id;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }
        const {queued, queue_item_id} = data;

        return new CurrentUserModel(queued, queue_item_id);
    }
}

export {CurrentUserModel};