import {ChannelsModel} from './channels_model';
import {Arrays} from "helpers/arrays";

class LiveFeedModel {

    constructor(channels) {
        this.channels = channels;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const channels = Arrays.fromJSONArray(ChannelsModel, data.channels);

        return new LiveFeedModel(channels);
    }
}

export {LiveFeedModel};