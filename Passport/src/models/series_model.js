import {ContentModelSelector} from '../helpers/content_model_selector';
import {EpisodeContentModel} from './episode_content_model';
import {CollectionItem} from './collection_item';
import {Arrays} from '../helpers/arrays';

class SeriesModel {

    constructor(id, title, description, external_url, next_episode, seasons, episodes) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.title = title;
        this.description = description;
        this.external_url = external_url;
        this.next_episode = next_episode;
        this.seasons = seasons;
        this.episodes = episodes;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {id, title, description, external_url} = data.series;
        const next_episode = EpisodeContentModel.fromJSON(data.series.next_episode);
        const seasons = Arrays.fromJSONArray(CollectionItem, data.series.seasons);
        const episodes = Arrays.fromJSONArray(CollectionItem, data.series.episodes);

        return new SeriesModel(id, title, description, external_url, next_episode, seasons, episodes);
    }
}

export {SeriesModel};