import {Arrays} from "helpers/arrays";
import {CollectionItem} from "models/collection_item";

class ScheduleModel {

    constructor(name, short_name, logo, items) {
        this.name = name;
        this.short_name = short_name;
        this.logo = logo;
        this.items = items;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {name, short_name, logo} = data.channel;
        const items = Arrays.fromJSONArray(CollectionItem, data.channel.items);

        return new ScheduleModel(name, short_name, logo, items);
    }
}

export {ScheduleModel};