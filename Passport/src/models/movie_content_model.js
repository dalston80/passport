import {EntitlementsModel} from './entitlements_model';

class MovieContentModel {

    constructor(id, title, thumbnail, short_name, release_year, mpaa_rating, duration, rotten_tomatoes_review, background, banner, entitlements) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.title = title;
        this.thumbnail = thumbnail;
        this.short_name = short_name;
        this.release_year = release_year;
        this.mpaa_rating = mpaa_rating;
        this.duration = duration;
        this.rotten_tomatoes_review = rotten_tomatoes_review;
        this.background = background;
        this.banner = banner;
        this.entitlements = entitlements;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }
        const {id, title, thumbnail, short_name, release_year, mpaa_rating, duration, rotten_tomatoes_review, background, banner, entitlements} = data;

        return new MovieContentModel(id, title, thumbnail, short_name, release_year, mpaa_rating, duration, rotten_tomatoes_review, background, banner, entitlements);
    }
}

export {MovieContentModel};