import {StuntItem} from "models/stunt_item";

class StuntModel {

    constructor(meta, stunt) {
        this.meta = meta;
        this.stunt = stunt;
    }

    static fromJSON(data) {
        const {meta} = data;
        const stunt = StuntItem.fromJSON(data.stunt);

        return new StuntModel(meta, stunt);
    }
}

export {StuntModel};