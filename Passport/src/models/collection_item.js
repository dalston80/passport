import {ContentModelSelector} from '../helpers/content_model_selector';

class CollectionItem {

    constructor(type, content, action, target) {
        this.type = type;
        this.content = content;
        this.action = action;
        this.target = target;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {type, action, target} = data;
        const content = ContentModelSelector.getModel(type, data.content);

        return new CollectionItem(type, content, action, target);
    }
}

export {CollectionItem};