class UserHistoryItemModel {

    constructor(id, created_at, user_id, item_id, item_type) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.created_at = created_at;
        this.user_id = user_id;
        this.item_id = item_id;
        this.item_type = item_type;
    }

    static fromJSON(data) {
        const {id, created_at, user_id, item_id, item_type} = data;
        
        return new UserHistoryItemModel(id, created_at, user_id, item_id, item_type);
    }
}

export {UserHistoryItemModel};