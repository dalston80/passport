import {MovieListItem} from './movie_list_item';

class ChannelsModel {

    constructor(name, short_name, path, logo, timer, now_playing, up_next) {
        this.name = name;
        this.short_name = short_name;
        this.path = path;
        this.logo = logo;
        this.timer = timer;
        this.now_playing = now_playing;
        this.up_next = up_next;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {name, short_name, path, logo, timer} = data;
        const now_playing = MovieListItem.fromJSON(data.now_playing);
        const up_next = MovieListItem.fromJSON(data.up_next);

        return new ChannelsModel(name, short_name, path, logo, timer, now_playing, up_next);
    }
}

export {ChannelsModel};