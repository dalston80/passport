class QueueStatusModel {

    constructor(queued, queue_item_id) {
        this.queued = queued;
        this.queue_item_id = queue_item_id;
    }

    static fromJSON(data) {
        const {queued, queue_item_id} = data.movie;

        return new QueueStatusModel(queued, queue_item_id);
    }
}

export {QueueStatusModel};