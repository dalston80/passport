import {PostersModel} from "models/posters_model";
import {VideoItem} from "models/video_item";
import {EntitlementsModel} from "models/entitlements_model";
import {CreditsModel} from "models/credits_model";
import {ExtrasMovieModel} from "models/extras_movie_model";
import {CurrentUserModel} from "models/current_user_model";

class MovieModel {

    constructor(id, title, mpaa_rating, release_year, duration, short_name, genres, trailer, rotten_tomatoes_review, entitlements, synopsis, posters, extras, credits, current_user) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.title = title;
        this.mpaa_rating = mpaa_rating;
        this.release_year = release_year;
        this.duration = duration;
        this.short_name = short_name;
        this.genres = genres;
        this.trailer = trailer;
        this.rotten_tomatoes_review = rotten_tomatoes_review;
        this.entitlements = entitlements;
        this.synopsis = synopsis;
        this.posters = posters;
        this.extras = extras;
        this.credits = credits;
        this.current_user = current_user;
    }

    static fromJSON(data) {
        const {id, title, mpaa_rating, release_year, duration, short_name, genres, synopsis, rotten_tomatoes_review} = data.movie;
        const trailer = VideoItem.fromJSON(data.movie.trailer);
        const entitlements = EntitlementsModel.fromJSON(data.movie.entitlements);
        const posters = PostersModel.fromJSON(data.movie.posters);
        const extras = ExtrasMovieModel.fromJSON(data.movie.extras);
        const credits = CreditsModel.fromJSON(data.movie.credits);
        const current_user = CurrentUserModel.fromJSON(data.movie.current_user);

        return new MovieModel(id, title, mpaa_rating, release_year, duration, short_name, genres, trailer, rotten_tomatoes_review, entitlements, synopsis, posters, extras, credits, current_user);
    }
}

export {MovieModel};