class UserQueueItemModel {

    constructor(id, movie_id, ordering, created_at) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.movie_id = movie_id;
        this.ordering = ordering;
        this.created_at = created_at;
    }

    static fromJSON(data) {
        const {id, movie_id, ordering, created_at} = data;

        return new UserQueueItemModel(id, movie_id, ordering, created_at);
    }
}

export {UserQueueItemModel};