class VideoItem {

    constructor(id, title, duration, description, hlspath, poster) {
        if (!id) {
            throw new Error('id is required!');
        }

        this.id = id;
        this.title = title;
        this.duration = duration;
        this.description = description;
        this.hlspath = hlspath;
        this.poster = poster;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }
        const {id, title, description, duration, hlspath, poster} = data;

        return new VideoItem(id, title, duration, description, hlspath, poster);
    }
}

export {VideoItem};