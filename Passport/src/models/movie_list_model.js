import {Arrays} from "helpers/arrays";
import {MovieListItem} from "models/movie_list_item";

class MovieListModel {

    constructor(meta, movies) {
        this.meta = meta;
        this.movies = movies;
    }

    static fromJSON(data) {
        const {meta} = data;
        const movies = Arrays.fromJSONArray(MovieListItem, data.movies);

        return new MovieListModel(meta, movies);
    }
}

export {MovieListModel};