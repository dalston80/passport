import {Arrays} from "helpers/arrays";
import {MovieListItem} from "./movie_list_item";

class StuntItem {

    constructor(id, name, movies) {
        if (!id) {
            throw new Error('id is required!');
        }
        if (!movies) {
            movies = [];
        }
        this.id = id;
        this.name = name;
        this.movies = movies;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {id, name} = data;
        const movies = Arrays.fromJSONArray(MovieListItem, data.movies);

        return new StuntItem(id, name, movies);
    }
}

export {StuntItem};