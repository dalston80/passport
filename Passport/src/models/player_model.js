class PlayerModel {

    constructor(url, thumb) {
        this.url = url;
        this.thumb = thumb;
    }

    static fromJSON(data) {
        const {url, thumb} = data;

        return new PlayerModel(url, thumb);
    }
}

export {PlayerModel};