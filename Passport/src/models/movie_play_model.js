import {EntitlementsModel} from "./entitlements_model";

class MoviePlayModel {

    constructor(id, position, playlist, expires_at, entitlements) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.position = position;
        this.playlist = playlist;
        this.expires_at = expires_at;
        this.entitlements = entitlements;
    }

    static fromJSON(data) {
        const {id, position, playlist, expires_at, entitlements} = data;
        //const entitlements = EntitlementsModel.fromJSON(data.movie.entitlements);

        return new MoviePlayModel(id, position, playlist, expires_at, entitlements);
    }
}

export {MoviePlayModel};