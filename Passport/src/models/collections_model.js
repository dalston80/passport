class CollectionsModel {

    constructor(meta, collections) {
        this.meta = meta;
        this.collections = collections;
    }

    static fromJSON(data) {
        const {meta, collections} = data;

        return new CollectionsModel(meta, collections);
    }
}

export {CollectionsModel};