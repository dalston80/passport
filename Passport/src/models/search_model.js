import {Arrays} from "helpers/arrays";
import {SearchCollectionItemModel} from "models/search_collection_item_model";

class SearchModel {

    constructor(meta, items) {
        this.meta = meta;
        this.items = items;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {meta} = data.search;
        const items = Arrays.fromJSONArray(SearchCollectionItemModel, data.search.items);

        return new SearchModel(meta, items);
    }
}

export {SearchModel};