import {CollectionItem} from './collection_item';
import {ContentModelSelector} from '../helpers/content_model_selector';

class SearchCollectionItemModel extends CollectionItem {

    constructor(type, content, action, target, current_user) {
        super(type, content, action, target);
        this.current_user = current_user;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {type, action, target, current_user} = data;
        const content = ContentModelSelector.getModel(type, data.content);

        return new SearchCollectionItemModel(type, content, action, target, current_user);
    }
}

export {SearchCollectionItemModel};