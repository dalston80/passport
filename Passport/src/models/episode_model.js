import {EpisodeContentModel} from './episode_content_model';
import {MovieModel} from './movie_model';
import {CollectionContentModel} from './collection_content_model';

class EpisodeModel {

    constructor(id, description, next_episode, movie, season, series) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.description = description;
        this.next_episode = next_episode;
        this.movie = movie;
        this.season = season;
        this.series = series;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {id, description} = data.episode;
        const next_episode = EpisodeContentModel.fromJSON(data.episode.next_episode);
        const movie = MovieModel.fromJSON(data.episode);
        const season = CollectionContentModel.fromJSON(data.episode.season);
        const series = CollectionContentModel.fromJSON(data.episode.series);

        return new EpisodeModel(id, description, next_episode, movie, season, series);
    }
}

export {EpisodeModel};