import {CollectionItem} from './collection_item';
import {ContentModelSelector} from '../helpers/content_model_selector';

class HistoryCollectionItemModel extends CollectionItem {

    constructor(type, content, action, target, id) {
        super(type, content, action, target);

        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {type, action, target, id} = data;
        const content = ContentModelSelector.getModel(type, data.content);

        return new HistoryCollectionItemModel(type, content, action, target, id);
    }
}

export {HistoryCollectionItemModel};