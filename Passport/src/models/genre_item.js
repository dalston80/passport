import {Arrays} from "helpers/arrays";
import {MovieListItem} from '../models/movie_list_item';

class GenreItem {

    constructor(id, name, movies) {
        if (!id) {
            throw new Error('id is required!');
        }
        if (!movies) {
            movies = [];
        }
        this.id = id;
        this.name = name;
        this.movies = movies;
    }

    static fromJSON(data) {
        const {id, name} = data;
        const movies = Arrays.fromJSONArray(MovieListItem, data.movies);

        return new GenreItem(id, name, movies);
    }
}

export {GenreItem};