import {Arrays} from "helpers/arrays";
import {CollectionItem} from "models/collection_item";

class CollectionModel {

    constructor(id, meta, title, description, items) {
        if (!id) {
            throw new Error('id is required!');
        }
        if (!items) {
            items = [];
        }
        this.id = id;
        this.title = title;
        this.description = description;
        this.meta = meta;
        this.items = items;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {meta, collection} = data;
        const {id, title, description} = collection;
        const items = Arrays.fromJSONArray(CollectionItem, collection.items);

        return new CollectionModel(id, meta, title, description, items);
    }
}

export {CollectionModel};