import {Arrays} from "helpers/arrays";
import {GenreItem} from "./genre_item";

class GenreModel {

    constructor(meta, genre) {
        this.meta = meta;
        this.genre = genre;
    }

    static fromJSON(data) {
        const {meta} = data;
        const genre = GenreItem.fromJSON(data.genre);

        return new GenreModel(meta, genre);
    }
}

export {GenreModel};