class UserModel {

    constructor(user_id, mso_name, msologos, service_level, attributes, ovx_enabled, entitled, entitlement_status) {
        this.user_id = user_id;
        this.mso_name = mso_name;
        this.msologos = msologos;
        this.service_level = service_level;
        this.attributes = attributes;
        this.ovx_enabled = ovx_enabled;
        this.entitled = entitled;
        this.entitlement_status = entitlement_status;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {user_id, mso_name, msologos, service_level, attributes, ovx_enabled, entitled, entitlement_status} = data.user_session;

        return new UserModel(user_id, mso_name, msologos, service_level, attributes, ovx_enabled, entitled, entitlement_status);
    }
}

export {UserModel};