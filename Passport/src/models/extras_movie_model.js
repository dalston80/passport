class ExtrasMovieModel {

    constructor(images, videos) {
        this.images = images;
        this.videos = videos;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }
        const {images, videos} = data;

        return new ExtrasMovieModel(images, videos);
    }
}

export {ExtrasMovieModel};