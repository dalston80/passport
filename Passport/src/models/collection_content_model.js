class CollectionContentModel {

    constructor(id, title, thumbnail, name, url) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.title = title;
        this.thumbnail = thumbnail;
        this.name = name;
        this.url = url;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {id, title, thumbnail, name, url} = data;

        return new CollectionContentModel(id, title, thumbnail, name, url);
    }
}

export {CollectionContentModel};