class EpisodeContentModel {

    constructor(id, title, thumbnail, short_name, release_year, mpaa_rating, duration, rotten_tomatoes_review, background, next_episode, season_id) {
        if (!id) {
            throw new Error('id is required!');
        }
        this.id = id;
        this.title = title;
        this.thumbnail = thumbnail;
        this.short_name = short_name;
        this.release_year = release_year;
        this.mpaa_rating = mpaa_rating;
        this.duration = duration;
        this.rotten_tomatoes_review = rotten_tomatoes_review;
        this.background = background;
        this.next_episode = next_episode;
        this.season_id = season_id;
    }

    static fromJSON(data) {
        if (!data) {
            return undefined;
        }

        const {id, title, thumbnail, short_name, release_year, mpaa_rating, duration, rotten_tomatoes_review, background, next_episode, season_id} = data;

        return new EpisodeContentModel(id, title, thumbnail, short_name, release_year, mpaa_rating, duration, rotten_tomatoes_review, background, next_episode, season_id);
    }
}

export {EpisodeContentModel};