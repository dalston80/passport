import {ApiRequest} from "../net/api_request";
import {DEFAULT_PAGE, DEFAULT_PAGE_SIZE} from "../helpers/pagination";
import {MoviePlayModel} from "../models/movie_play_model";
import {MovieModel} from "../models/movie_model";
import {MovieListModel} from "../models/movie_list_model";
import {MoviePlayheadPositionModel} from "../models/movie_playhead_position_model";

class MovieController {
    
    getAllMovies(page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE) {
        const path = 'movies';
        return ApiRequest.doPaginatedGetRequest(path, MovieListModel, page, page_size);
    }

    playMovie(movie_id) {
        const path = `movies/${movie_id}/play`;
        return ApiRequest.doGetRequest(path, MoviePlayModel)
    }

    showMovie(id) {
        const path = `movies/${id}`;
        return ApiRequest.doGetRequest(path, MovieModel)
    }

    getPlayheadPosition(movie_id) {
        const path = `movies/${movie_id}/playhead`;
        return ApiRequest.doGetRequest(path, MoviePlayheadPositionModel);
    }

    updatePlayheadPosition(movie_id, playhead) {
        const playheadDetails = JSON.stringify(playhead);
        const path = `movies/${movie_id}/playhead`;
        return ApiRequest.doPatchRequest(path, playheadDetails);
    }
}

export {MovieController};