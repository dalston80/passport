import {ApiRequest} from "../net/api_request";
import {DEFAULT_PAGE, DEFAULT_PAGE_SIZE} from "../helpers/pagination";
import {ExtrasModel} from "../models/extras_model";
import {ExtraModel} from "../models/extra_model";

class ExtrasController {

    getFeatured() {
        const path = 'extras/featured';
        return ApiRequest.doGetRequest(path, ExtraModel);
    }

    getExtrasItems(id) {
        const path = `extras/${id}/items`;
        return ApiRequest.doGetRequest(path, ExtraModel);
    }

    getExtrasList(page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE) {
        const path = 'extras';
        return ApiRequest.doPaginatedGetRequest(path, ExtrasModel, page, page_size);
    }

    getExtra(id) {
        const path = `extras/${id}`;
        return ApiRequest.doGetRequest(path, ExtrasModel);
    }
}

export {ExtrasController};