import {ApiRequest} from "../net/api_request";
import {SeriesModel} from '../models/series_model';

class SeriesController {

    getSeries(id) {
        const path = `series/${id}`;
        return ApiRequest.doGetRequest(path, SeriesModel);
    }
}

export {SeriesController};