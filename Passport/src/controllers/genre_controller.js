import {ApiRequest} from "../net/api_request";
import {DEFAULT_PAGE, DEFAULT_PAGE_SIZE} from "../helpers/pagination";
import {GenresModel} from "../models/genres_model";
import {GenreModel} from "../models/genre_model";

class GenreController {

    getAllGenres() {
        const path = 'genres/all';
        return ApiRequest.doGetRequest(path, GenresModel);
    }

    getGenreList() {
        const path = 'genres';
        return ApiRequest.doGetRequest(path, GenresModel);
    }

    getGenreWithMovies(id, page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE) {
        const path = `genres/${id}/movies`;
        return ApiRequest.doPaginatedGetRequest(path, GenreModel, page, page_size);
    }

    getGenre(id) {
        const path = `genres/${id}`;
        return ApiRequest.doGetRequest(path, GenreModel);
    }

}

export {GenreController};