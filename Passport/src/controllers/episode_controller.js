import {ApiRequest} from "../net/api_request";
import {EpisodeModel} from '../models/episode_model';

class EpisodeController {

    getEpisode(id) {
        const path = `episodes/${id}`;
        return ApiRequest.doGetRequest(path, EpisodeModel);
    }

}

export {EpisodeController};

