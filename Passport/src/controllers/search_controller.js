import {ApiRequest} from "../net/api_request";
import {DEFAULT_PAGE, DEFAULT_PAGE_SIZE} from "../helpers/pagination";
import {SearchModel} from "../models/search_model";
import {MovieListModel} from "../models/movie_list_model";

class SearchController {

    searchByShortName(shortname) {
        const path = `search/short_name/${shortname}`;
        return ApiRequest.doGetRequest(path, MovieListModel)
    }

    searchByLetter(letter, page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE, in_window_only = 0) {
        const path = `search/alpha/${letter}`;
        return ApiRequest.doPaginatedGetRequest(path, MovieListModel, page, page_size, `in_window_only=${in_window_only}`);
    }

    searchByTerm(term, page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE, in_window_only = 0) {
        // TODO: Consider looking into using an API boolean formatter here, e.g something like `function booleanFormatter(bool) { return 1 : true ? false }`
        const path = `search/${term}`;
        return ApiRequest.doPaginatedGetRequest(path, SearchModel, page, page_size, `in_window_only=${in_window_only}`);
    }
}

export {SearchController};