import {ApiRequest} from "../net/api_request";
import {ScheduleModel} from '../models/schedule_model';
import {LiveFeedModel} from '../models/live_feed_model';
import {EpisodeModel} from '../models/episode_model';
import {SeriesModel} from '../models/series_model';

class ChannelsController {

    getChannelDetails(short_name, days, offset) {
        const path = `channels/${short_name}/days/${days}/offset/${offset}`;
        return ApiRequest.doGetRequest(path, ScheduleModel);
    }

    getLiveFeed() {
        const path = 'channels';
        return ApiRequest.doGetRequest(path, LiveFeedModel);
    }
}

export {ChannelsController};