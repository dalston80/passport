import {ApiRequest} from "../net/api_request";
import {DEFAULT_PAGE, DEFAULT_PAGE_SIZE} from "../helpers/pagination";
import {UserQueueModel} from "../models/user_queue_model";
import {QueueStatusModel} from "../models/queue_status_model";
import {UserQueueItemModel} from "../models/user_queue_item_model";

class QueueController {

    getQueue(page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE) {
        const path = 'queue';
        return ApiRequest.doPaginatedGetRequest(path, UserQueueModel, page, page_size);
    }

    getQueueMovieStatus(movie_id) {
        const path = `movies/${movie_id}/queuestatus`;
        return ApiRequest.doGetRequest(path, QueueStatusModel);
    }

    addToQueue(movie_id) {
        const path = Config.BASE_URL + `queue/${movie_id}`;
        return ApiRequest.doPostRequest(path, UserQueueItemModel);
    }

    deleteQueue() {
        const path = 'queue';
        return ApiRequest.doDeleteRequest(path);
    }

    deleteQueueItemById(queue_item_ids) {
        const path = 'queue';
        const json_payload = JSON.stringify(queue_item_ids);
        ApiRequest.doDeleteRequestWithPayload(path, json_payload);
    }
}

export {QueueController};