import {ApiRequest} from "../net/api_request";
import {DEFAULT_PAGE, DEFAULT_PAGE_SIZE} from "../helpers/pagination";
import {StuntsModel} from "../models/stunts_model";
import {StuntModel} from "../models/stunt_model";

class StuntsController {

    getStuntList(page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE) {
        const path = 'stunts';
        return ApiRequest.doPaginatedGetRequest(path, StuntsModel, page, page_size);
    }

    getStuntsWithMovies(id, page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE) {
        const path = `stunts/${id}/movies`;
        return ApiRequest.doPaginatedGetRequest(path, StuntModel, page, page_size);
    }

    getStunt(id) {
        const path = `stunts/${id}`;
        return ApiRequest.doGetRequest(path, StuntModel);
    }
}

export {StuntsController};