import {ApiRequest} from "../net/api_request";
import {MSOListModel} from '../models/mso_list_model';
import {Session} from '../session';

class AuthController {

    getLoginURL(mso_name, return_url) {
        const session_token = Session.getToken();
        const path = Config.BASE_URL + `user_session/new?mso_name=${mso_name}&return_url=${return_url}&X-Session-Token=` + session_token;
        return path;
    }

    getMSOList(free_trial = 0) {
        const path = `msos/all/?free_trial=${free_trial}`;
        return ApiRequest.doGetRequest(path, MSOListModel);
    }
}

export {AuthController};