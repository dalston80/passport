import {ApiRequest} from "../net/api_request";
import {DEFAULT_PAGE, DEFAULT_PAGE_SIZE} from "../helpers/pagination";
import {CollectionModel} from "../models/collection_model";
import {CollectionsModel} from "../models/collections_model";

class CollectionController {

    getCollectionItems(id, page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE) {
        const path = `collections/${id}/items`;
        return ApiRequest.doPaginatedGetRequest(path, CollectionModel, page, page_size);
    }

    getCollection(id) {
        const path = `collections/${id}`;
        return ApiRequest.doGetRequest(path, CollectionModel);
    }

    getFeatured() {
        const path = 'collections/featured';
        return ApiRequest.doGetRequest(path, CollectionModel);
    }

    getLandingPage() {
        const path = 'collections/landing_page';
        return ApiRequest.doGetRequest(path, CollectionModel);
    }

    getList(page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE) {
        const path = 'collections';
        return ApiRequest.doPaginatedGetRequest(path, CollectionsModel, page, page_size);
    }

    getOriginals() {
        const path = 'collections/originals';
        return ApiRequest.doGetRequest(path, CollectionModel);
    }

}

export {CollectionController};