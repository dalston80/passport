import {ApiRequest} from "../net/api_request";
import {UserModel} from '../models/user_model';

class UserController {

    createFreeTrialAccount(free_trial, user, user_profile) {
        const path = 'users';

        const freeTrialObj = {
            free_trial: free_trial,
            user: user,
            user_profile: user_profile
        };

        const json_payload = JSON.stringify(freeTrialObj);
        return ApiRequest.doPostRequest(path, UserModel, json_payload);
    }
}

export {UserController};