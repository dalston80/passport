import {ApiRequest} from "../net/api_request";
import {DEFAULT_PAGE, DEFAULT_PAGE_SIZE} from "../helpers/pagination";
import {UserHistoryModel} from "../models/user_history_model";

class HistoryController {

    getHistory(page = DEFAULT_PAGE, page_size = DEFAULT_PAGE_SIZE) {
        const path = 'history';
        return ApiRequest.doPaginatedGetRequest(path, UserHistoryModel, page, page_size);
    }

    deleteAllHistory() {
        const path = 'history';
        ApiRequest.doDeleteRequest(path);
    }

    deleteHistoryItemById(history_ids) {
        const path = 'history';
        const json_payload = JSON.stringify(history_ids);
        ApiRequest.doDeleteRequest(path, json_payload);
    }
}

export {HistoryController};