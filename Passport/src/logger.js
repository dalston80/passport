import {QueryString} from 'net/query_string';

const logger = require('loglevel');

class Logger {

    static setLevel(level = 'info', persist = false) {
        const vars = QueryString.getVars();
        const debug = vars.debug;
        const debugLevel = parseInt(vars.level, 10) || vars.levelname;

        if (debug && debugLevel) {
            logger.setLevel(debugLevel, persist);
        } else if (debug) {
            logger.setLevel('debug', persist);
        } else {
            logger.setLevel(level, persist);
        }
    }

    static debug(msg) {
        logger.debug(msg);
    }

    static info(msg) {
        logger.info(msg);
    }

    static warn(msg) {
        logger.warn(msg);
    }

    static error(msg) {
        logger.error(msg);
    }

    static trace(msg) {
        logger.trace(msg);
    }
}
export {Logger};